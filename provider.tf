terraform {
  required_providers {
    aci = {
      source = "CiscoDevNet/aci"
    }
  }
}

provider "vsphere" {
  allow_unverified_ssl = true
}

provider "aci" {
  username = var.APIC_USERNAME
  password = var.APIC_PASSWORD
  url      = var.APIC_URL
  insecure = true
}
