# Terraform-Module: VM Medium

## Requirements: providers
- [hashicorp/terraform-provider-vsphere](https://registry.terraform.io/providers/hashicorp/vsphere/latest)

## Requirements: TF Cloud secret or tfvars
- vSphere Endpoint (vCenter URL)
- vSphere User
- vSphere Password


## Spec
  - vCPU:      2
  - Memory:    4GB
  - Storage:   64GB
  - PortGroup: 1 (IPv4 Address)

## Variables
| Name | Type | Descriptions |
| ----------- | ----------- | ----------- |
| datacenter | String | vSphere Datacenter Name |
| cluster | String | vSphere Host Cluster Name |
| vm_template | String | Template Name |
| datastore | String | Datastore Name |
| host_name | String | Host Name |
| portgroup | String | Port-Group Name |
| address_v4 | String | IPv4 address included subnet (ex. "192.168.1.1/24") |
| gateway_v4 | String | Gateway's IPv4 address |