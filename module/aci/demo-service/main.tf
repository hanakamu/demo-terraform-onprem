terraform {
  required_providers {
    aci = {
      source = "CiscoDevNet/aci"
    }
  }
}

resource "aci_tenant" "tenant" {
  name        = var.tenant_name
  description = var.tenant_description
}

resource "aci_vrf" "vrf" {
  tenant_dn = aci_tenant.tenant.id
  name      = "main_vrf"
}

resource "aci_bridge_domain" "web_bd" {
  tenant_dn          = aci_tenant.tenant.id
  name               = "web_bd"
  relation_fv_rs_ctx = aci_vrf.vrf.id
}


resource "aci_subnet" "web_subnet" {
  parent_dn = aci_bridge_domain.web_bd.id
  ip        = var.web_bd_subnet
}

resource "aci_bridge_domain" "app_bd" {
  tenant_dn          = aci_tenant.tenant.id
  name               = "app_bd"
  relation_fv_rs_ctx = aci_vrf.vrf.id
}


resource "aci_subnet" "app_subnet" {
  parent_dn = aci_bridge_domain.app_bd.id
  ip        = var.app_bd_subnet
}

resource "aci_bridge_domain" "db_bd" {
  tenant_dn          = aci_tenant.tenant.id
  name               = "db_bd"
  relation_fv_rs_ctx = aci_vrf.vrf.id
}

resource "aci_subnet" "db_subnet" {
  parent_dn = aci_bridge_domain.db_bd.id
  ip        = var.db_bd_subnet
}

data "aci_vmm_domain" "vmm_domain" {
  provider_profile_dn = "uni/vmmp-VMware"
  name                = var.vmm_domain
}

resource "aci_application_profile" "ap" {
  tenant_dn = aci_tenant.tenant.id
  name      = "demo_ap"
}

resource "aci_application_epg" "web_epg" {
  application_profile_dn = aci_application_profile.ap.id
  name                   = "web_epg"
  relation_fv_rs_bd      = aci_bridge_domain.web_bd.id
}

resource "aci_application_epg" "app_epg" {
  application_profile_dn = aci_application_profile.ap.id
  name                   = "app_epg"
  relation_fv_rs_bd      = aci_bridge_domain.app_bd.id
}

resource "aci_application_epg" "db_epg" {
  application_profile_dn = aci_application_profile.ap.id
  name                   = "db_epg"
  relation_fv_rs_bd      = aci_bridge_domain.db_bd.id
}

resource "aci_epg_to_domain" "web_bd_to_epg" {
  application_epg_dn = aci_application_epg.web_epg.id
  tdn                = data.aci_vmm_domain.vmm_domain.id
}

resource "aci_epg_to_domain" "app_bd_to_epg" {
  application_epg_dn = aci_application_epg.app_epg.id
  tdn                = data.aci_vmm_domain.vmm_domain.id
}

resource "aci_epg_to_domain" "db_bd_to_epg" {
  application_epg_dn = aci_application_epg.db_epg.id
  tdn                = data.aci_vmm_domain.vmm_domain.id
}

resource "aci_contract" "app_to_web_contract" {
  tenant_dn = aci_tenant.tenant.id
  name      = "app_to_web"
}

resource "aci_contract" "db_to_app_contract" {
  tenant_dn = aci_tenant.tenant.id
  name      = "db_to_app"
}

resource "aci_filter" "allow_https" {
  tenant_dn = aci_tenant.tenant.id
  name      = "allow_https"
}

resource "aci_filter" "allow_mysql" {
  tenant_dn = aci_tenant.tenant.id
  name      = "allow_mysql"
}

resource "aci_filter" "allow_icmp" {
  tenant_dn = aci_tenant.tenant.id
  name      = "allow_icmp"
}

resource "aci_filter_entry" "https" {
  filter_dn   = aci_filter.allow_https.id
  name        = "tcp"
  d_from_port = "443"
  d_to_port   = "443"
  prot        = "tcp"
  ether_t     = "ip"
}

resource "aci_filter_entry" "mysql" {
  filter_dn   = aci_filter.allow_mysql.id
  name        = "mysql"
  d_from_port = "3306"
  d_to_port   = "3306"
  prot        = "tcp"
  ether_t     = "ip"
}

resource "aci_filter_entry" "icmp" {
  filter_dn = aci_filter.allow_icmp.id
  name      = "icmp"
  prot      = "icmp"
  ether_t   = "ip"
}

#Contract's Subject Creation
resource "aci_contract_subject" "app_to_web_subject" {
  contract_dn                  = aci_contract.app_to_web_contract.id
  name                         = "app_to_web_subject"
  relation_vz_rs_subj_filt_att = [aci_filter.allow_icmp.id, aci_filter.allow_https.id]
}

resource "aci_contract_subject" "db_to_app_subject" {
  contract_dn                  = aci_contract.db_to_app_contract.id
  name                         = "db_to_app_subject"
  relation_vz_rs_subj_filt_att = [aci_filter.allow_icmp.id, aci_filter.allow_mysql.id]
}

# app_to_web contract association with WEB_EPG and APP_EPG
resource "aci_epg_to_contract" "app_to_web_consumer" {
  application_epg_dn = aci_application_epg.web_epg.id
  contract_dn        = aci_contract.app_to_web_contract.id
  contract_type      = "consumer"
}

resource "aci_epg_to_contract" "app_to_web_provider" {
  application_epg_dn = aci_application_epg.app_epg.id
  contract_dn        = aci_contract.app_to_web_contract.id
  contract_type      = "provider"
}

# db_to_app contract association with APP_EPG and DB_EPG
resource "aci_epg_to_contract" "db_to_app_consumer" {
  application_epg_dn = aci_application_epg.app_epg.id
  contract_dn        = aci_contract.db_to_app_contract.id
  contract_type      = "consumer"
}

resource "aci_epg_to_contract" "db_to_app_provider" {
  application_epg_dn = aci_application_epg.db_epg.id
  contract_dn        = aci_contract.db_to_app_contract.id
  contract_type      = "provider"
}


