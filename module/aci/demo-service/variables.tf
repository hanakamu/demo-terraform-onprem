variable "tenant_name" {
  type = string
}

variable "tenant_description" {
  type    = string
  default = "demo_ist_aci"
}

variable "vmm_domain" {
  type = string
}

variable "web_bd_subnet" {
  type = string
}

variable "app_bd_subnet" {
  type = string
}

variable "db_bd_subnet" {
  type = string
}