# For ACI Provider
variable "APIC_USERNAME" {
  type = string
}

variable "APIC_PASSWORD" {
  type = string
}

variable "APIC_URL" {
  type = string
}